% ---------------- SERVER ---------------- %
\section{Server}
\label{sec:server}

% ---------------- OVERVIEW ---------------- %
\subsection{Overview}
\label{sub:server-overview}

A server is a node in an AXIoE network that is capable of executing transactions sent by a client. The server implementation is intended to be simple with the goal that few resources are required for implementation. This facilitates their use in a variety of embedded devices. A server is intended to be a single threaded state machine, such that operations execute in a defined order.

A server maps all transactions to a virtual memory space called the AXIoE Bus. The AXIoE bus may be implemented as an actual physical bus in a logic device with peripherals located at specified memory addresses. Alternatively the AXIoE Bus addresses may be tokenized and used abstractly, for instance procedures and their parameters may be associated with particular addresses.

% ---------------- PARAMETERS ---------------- %
\subsection{Parameters}
\label{sub:server-parameters}

The server has the following parameters:
\begin{enumsub}
    \item
        Replay buffer length. This length shall be between 0 and 255.
    \item
        Maximum UDP payload size for \pkt{REQ} packets.
    \item
        Maximum UDP payload size for \pkt{CPL} packets.
    \item
        Indication of support for conditional transaction execution.
    \item
        Transaction parameters supported:
        \subitem Beat sizes supported,
        \subitem Maximum burst length, and
        \subitem Burst types supported.
    \item
        Vendor and Device identification numbers. See Section~\ref{sec:identification}.
    \item
        An Address of Interest.
\end{enumsub}
These parameters are advertised to Clients in a reply to a \pkt{PRB} packet. See Section~\ref{subsub:packet-prb-payload} for more information about the \pkt{PRB} payload fields.

% ---------------- COMPONENTS ---------------- %
\subsection{Components}
\label{sub:server-components}

The following sections describe components which are integral to an AXIoE server.

% ---------------- AXIoE BUS ---------------- %
\subsubsection{AXIoE Bus Master}
\label{subsub:server-axioe-bus}

The AXIoE bus is an abstract memory access bus based on the AMBA AXI4 bus architecture. The AXIoE bus is the target of the transactions carried by \pkt{REQ} packets. The AXIoE Bus Master component transforms the request transactions from a \pkt{REQ} packet into a completion transactions for the \pkt{CPL} packet.

%The purpose of defining this bus is provide a level of indirection between a broad ecosystem of AXIoE implementations and the actual AMBA/AXI4 bus specification. In brief, the AXIoE bus should be an abstract bus that can inherit any semantics/behaviors of an AMBA/AXI4 bus as required, but is not designed specifically to do so. The most obvious practical upshot of this definition is the explicit allowance for bus addresses to be tokenized by the server in any fashion seen fit.

%AXIoE clients interact with AXIoE servers by making transactions to and from the AXIoE bus. 
%The AXIoE bus is an abstract memory-mapped bus onto which parameters and procedures may be projected. Clients modify or query parameters by performing read and write transactions to the AXIoE bus addresses associated with those parameters. 

This specification allows the internal construction of the AXIoE bus to be implementation specific. In the simplest case, the AXIoE bus may be implemented as physical AMBA AXI-4 compliant bus.
Alternatively a microprocessor may tokenize addresses to be used abstractly, or any other implementation as seen fit.

The memory map on the AXIoE bus is implementation specific.\footnote{Consider using the Self-Describing Bus specification to provide memory map auto discovery.}

\begin{enumsubsub}
    \item
        The AXIoE bus shall inherit the semantics and behaviours from the AMBA AXI-4 standard. It should therefore be possible to translate AXIoE transaction payloads directly to an AXI-4 master.

        \begin{enumsubsub}

            \item
                This specification does not implement the full functionality of the AXI-4 protocol specification, and as such shall not be considered a direct substitute.

        \end{enumsubsub}
        
    \item \label{clause:server-cpl-build}
        The server shall execute a \pkt{REQ} transaction and build a \pkt{CPL} transaction according to the following:

        \begin{enumsubsub}
            \item
                The server shall set the \bit{CPL} bit in the transaction header.
            \item
                The remaining bits in the transaction header shall be set to the values found in the corresponding \pkt{REQ} header. 
            \item
                For a read completion, the server shall always provide the number and size of beats requested by the client.
                If there are insufficient values on the bus to meet the request, the server shall pad the size of the read completion data such that it equals the amount of data requested.
            \item
                For a write completion, the server shall not include any data.
            \item
                The server shall set the \field{STATUS} bits in the transaction header according to the outcome of the transaction execution.
            \end{enumsubsub}

\end{enumsubsub}


% ----------------Sequence Counter ----------------%
\subsubsection{Sequence Counter}
\label{subsub:server-sequence-counter}

AXIoE servers internally maintain an expected sequence number counter.
This counter is used to guarantee in-order delivery of packets and therefore transactions.
The sequence counter's value represents the \textbf{expected} sequence number of the next \pkt{REQ} packet.

%The following clauses define the rules of use for the server sequence counter.

\begin{enumsubsub}

    \item
        The server shall maintain an internal sequence counter to check incoming \pkt{REQ} packets against.
        This counter shall represent integers from 0 to 255 inclusive.
    
    \item
        At the start of a communication session the value of the sequence counter may be any integer between 0 and 255.
        A client can determine the server's expected sequence number using a probe packet to query the sequence number for the first request packet.

    \item
	The sequence counter will be incremented when the sequence number of a \pkt{REQ} packet matches the sequence counter value, except when the \pkt{IGN} bit is set.
        \begin{enumsub}
            \item
                The sequence counter shall be incremented as 
                \begin{equation}
                    S_{next} = (S_{curr} + 1) \mod 256
                \end{equation}

                where $S_{next}$ is the next sequence counter value, and $S_{curr}$ is the current sequence counter value.
        \end{enumsub}
        
    \item \label{clause:server-req-accept}
        The server shall enforce in order delivery by only accepting a request packet if either
        \begin{enumsub}
            \item
                the request packet sequence number matches the expected sequence counter value, or
            \item
                the request packet has the \pkt{IGN} bit set.
        \end{enumsub}

    \item
        \label{clause:server-out-of-order}
        If the server receives a \pkt{REQ} packet whose sequence number is not the expected sequence number, and the \pkt{IGN} bit is clear, it shall send a \pkt{NAK} packet. The sequence number of the \pkt{NAK} shall be the expected sequence number.
    
\end{enumsubsub}


% ---------------- REPLAY BUFFER ---------------- %
\subsubsection{Replay Buffer}
\label{subsub:server-replay-buffer}

The replay buffer contains the $N$ most recent \pkt{CPL} packets, where $N$ is the depth of the buffer.
\pkt{CPL} packets in the buffer are indexed by their sequence number.

A client can request the server retransmit a particular \pkt{CPL} packet by sending a \pkt{NAK} where the sequence number is that of the desired \pkt{CPL}.
The client should know from recent history that the \pkt{CPL} packet exists in the replay buffer.
If the server's replay buffer contains a packet which has the requested sequence number, the server shall transmit this packet to the requesting client.

\begin{enumsubsub}
    \item
        The server should maintain a replay buffer. The replay buffer shall contain the $MAX\_REPLAY\_BUF$ most recent completions, where $MAX\_REPLAY\_BUF$ is the length of the replay buffer. \begin{enumsubsub}
            \item
                The maximum advertised depth of the replay buffer is 255 packets. See Table~\ref{tab:axioe-prb-payload-tab}.
            \item
                The minimum advertised depth of the replay buffer is 0 packets. See Table~\ref{tab:axioe-prb-payload-tab}.
                \begin{example}
                    A server with a zero-length replay buffer cannot support lost \pkt{CPL} recovery.
                \end{example}

        \end{enumsubsub}
        
    \item
        The server shall insert new \pkt{CPL} packets into the buffer only if the \bit{IGN} bit is clear.

    \item
        The replay buffer shall be implemented as a write-in-order buffer.
        The addition of a new \pkt{CPL} packet shall cause the oldest packet to be removed from the buffer.\footnote{Equivalently a new \pkt{CPL} packet overwrites the oldest packet.}

    \item
	\label{clause:NAK-replay}
        When the server receives a \pkt{NAK} packet with sequence number $N$ it shall retrieve the \pkt{CPL} packet with sequence number $N$ from the replay buffer and retransmit that \pkt{CPL} packet.
        
        %\footnote{CPL is sent to the client that sent the NAK. Or should it be the original client that sent the REQ? I've implmented original, but the NAK client would be more robust. Allows easdropping on the server reads, but prevents a rouge client from interferring with another's session. On the other hand, receiving extra CPL won't hurt much. Eaiser ways for a rouge client to interfer ...}
        \begin{enumsubsub}
            \item
                If a matching packet cannot be found, the server should not transmit any packet.
            \item
                If two or more matching packets are found, the server should transmit the latest matching packet.
        \end{enumsubsub}
        \begin{example}
            This should not occur if the replay buffer is implemented such that it retires the old \pkt{CPL} packet for a particular sequence number when the next \pkt{REQ} with that sequence number is executed.
        \end{example}
    \item
        \pkt{CPL} packets shall be replayed to the Client from which the corresponding \pkt{REQ} was received.
        \begin{enumsubsub}
        \item
            The Server should check that the identity (MAC address, IP address, UDP port) of the Client sending the \pkt{NAK} packet is the same as the identity of the Client that sent the \pkt{REQ} packet. If they are different then the Server should not send the \pkt{CPL} packet.
        \end{enumsubsub}



\end{enumsubsub}


% ---------------- SERVER RESPONSIBILITIES ---------------- %
\subsection{Server Responsibilities}
\label{sub:server-responsibilities}

The following clauses define the responsibilities of an AXIoE server.

\begin{enumsub}
    \item
        In a UDP environment the server shall listen on a particular UDP port for incoming connections. The mapping of this port is implementation specific and outside the scope of this specification.
    
    \item
        The server shall reply to all compliant packets that arrive on its network interface up to the rate that its processing resources allow.
        
    \item 
        The server shall reply to a received \pkt{PRB} packet, with a \pkt{PRB} packet where the sequence number is set to the current value of the sequence counter.
    
    \item
        The server shall reply to a received \pkt{NAK} packet, with a \pkt{CPL} packet from the replay buffer according to clause \ref{clause:NAK-replay}.

    \item
        If the server accepts a received \pkt{REQ} packet according to clause \ref{clause:server-req-accept} then it shall process the one or more transactions through the AXIoE bus to build a \pkt{CPL} packet according to clause \ref{clause:server-cpl-build}. The resultant \pkt{CPL} packet shall be transmitted to the client.

    \item
        If the server does not accept a received \pkt{REQ} packet according to clause \ref{clause:server-req-accept} then it shall not process any transactions, and it shall transmit a \pkt{NAK} packet where the sequence number is set to the current value of the sequence counter.
    
    \item
        The server shall discard any packet it receives with the \bit{REQ} bit cleared.
        
\end{enumsub}


% ---------------- START OF LIFE ---------------- %
\subsubsection{Start-of-Life Behaviour}
\label{subsub:server-start-of-life}

The following clauses define the responsibilities of an AXIoE server specifically at the start of operation.

% TODO : A state machine diagram of the server?
\begin{enumsubsub}
    \item
        When the server is initialised it shall operate in a start-of-life context.
    \item
        The server shall exit the start-of-life context \textbf{immediately} if a \pkt{REQ} or \pkt{PRB} packet is received with the \bit{SOL\_ACK} bit set. The server shall then continue to execute the \pkt{REQ} or \pkt{PRB} packet as normal.
        \begin{enumsubsub}
            \item
                The server shall not exit the start-of-life context if the \bit{IGN} bit is set.
        \end{enumsubsub}

    \item
        The server may ignore the \pkt{SOL\_ACK} bit when not in the start-of-life context.
    \item
        In the start-of-life context the server shall set the \pkt{SOL\_SET} bit on all \pkt{CPL}, \pkt{NAK}, and \pkt{PRB} packets transmitted.
    \item
        \label{clause:server-sol-no-execute}
        In the start-of-life context the server shall not execute any \pkt{NAK} or \pkt{REQ} packets, except as per clause~\ref{clause:server-ign-exception}.
    \item
        The server shall ignore any incoming \pkt{NAK} replay packets while in the start-of-life context.
    \item
        \label{clause:server-ign-exception}
        In the start-of-life context, and if the server supports the use of the \pkt{IGN} bit, the server shall execute \pkt{REQ} packets with the \bit{IGN} bit set.
\end{enumsubsub}

