% ---------------- PROCEDURE CALL SIGNATURES ---------------- %
\section{Procedure Call Signatures}
\label{sec:procedure-call-signatures}

Each AXIoE procedure will have a unique signature, consisting of a name, ordered list of input parameters, and ordered list of output parameters.
Applications will typically define a group of procedures to form an application programming interface (API) for the remote machine.

A procedure signature can be written in two ways:
\begin{itemize}
    \item
        An \textit{abstract} procedure signature, a compact form written in a typical coding style.
        See section~\ref{subsub:signature-abstract-procedure-signature}.
    \item
        A \textit{concrete} procedure signature, in a tabular form written in a typical datasheet style that explicitly specifies the parameters of the procedure signature.
        See section~\ref{subsub:signature-concrete-procedure-signature}.
\end{itemize}

In defining the remote procedure signatures for an API, the designer should follow the conventions specified in this document.

% ---------------- ABSTRACT PROCEDURE SIGNATURE ---------------- %
\subsection{Abstract Procedure Signature}
\label{subsub:signature-abstract-procedure-signature}

The \textit{abstract} procedure signature is a notational device for representing a procedure call as a name coupled with a set of input and output parameters.
For example, the abstract procedure signature for an example procedure named PROCEDURE\_NAME with $N$ inputs and $M$ outputs is:

\begin{codeexample}
    [out\_0, *out\_1, \ldots, out\_M] = PROCEDURE\_EXAMPLE(in\_0, *in\_1, \ldots in\_N)
\end{codeexample}

The following clauses define the format of the abstract procedure signature notation. Following these rules will help in the automated generation of code.

\begin{enumsub}
    \item
        All identifiers shall use standard naming conventions, namely the alphanumeric character set and underscore (a-zA-Z0-9\_). Identifiers shall not begin with a numeric character (0-9).
    \item
        All white space characters shall be ignored, however use white space appropriately, and maintain a consistent style.
    \item
        The procedure signature shall specify all input and all output parameters of the procedure in the order that their associated transactions should be executed in.
        Carefully consider the order, as one order may simplify implementation at the remote machine.
    \item
        Output parameters shall be encapsulated by square brackets (\texttt{[ ]}) and appear at the beginning of the signature.
    \item
        The list of output parameters shall be comma-separated. The trailing comma may be omitted.
    \begin{example}
        Examples of valid signatures:
        \begin{codeexample}
            [out\_0,out\_1,] = TWO\_OUT (in\_0, \ldots, in\_N) \newline
            [out\_0, out\_1] = TWO\_OUT (in\_0, \ldots, in\_N) \newline
                    [out\_0,] = ONE\_OUT (in\_0, \ldots, in\_N) \newline
                    [out\_0 ] = ONE\_OUT (in\_0, \ldots, in\_N) \newline
                            [] = ZERO\_OUT(in\_0, \ldots, in\_N)
        \end{codeexample}
    \end{example}
    \item
        The name of the procedure shall follow the output parameter list. The closing square bracket (\texttt{]}) shall be separated from the procedure name by an equals (\texttt{=}) character.
    \item
        Input parameters shall be encapsulated by parenthesis (\texttt{( )}) and appear immediately after the procedure name.
    \item
        The list of input parameters shall be comma-separated. The trailing comma may be omitted.
    \item
        \textit{Immediate} flavour parameters are represented by plain names. %See Section~\ref{sub:translation-passing-input-parameters} for value parameter rules of use.
    \item
        \textit{Buffer} flavour parameters are represented by their name with a leading asterisk (\texttt{*}).% See Section~\ref{sub:translation-returning-results} for buffer parameter rules of use.
    \begin{example}
        Examples of valid signatures:
        \begin{codeexample}
            [out\_0] = TWO\_IN(in\_0, *in\_1,) \newline
            [out\_0] =TWO\_IN (*in\_0,in\_1) \newline
            [out\_0]= ONE\_IN (in\_0,) \newline
            [out\_0] = ONE\_IN(*in\_0) \newline
            [out\_0]=ZERO\_IN()
        \end{codeexample}
    \end{example}
\end{enumsub}

% ---------------- CONCRETE PROCEDURE SIGNATURE  ---------------- %
\subsection{Concrete Procedure Signature}
\label{subsub:signature-concrete-procedure-signature}


%The \textit{concrete} procedure signature represents a memory map on the AXIoE Bus.
The \textit{concrete procedure signature} lists the physical addresses on the server's AXIoE bus. Request transactions are directed to addresses specified in the concrete procedure signature. Each address maps to a parameter in the \textit{abstract procedure signature}. See Section~\ref{subsub:signature-abstract-procedure-signature}.

%Concrete procedure signatures are implemented on an AXIoE server. A concrete procedure signature relates parameters in the abstract procedure signature to physical or virtual addresses on the AXIoE bus. Each procedure has a base address, and one or more parameters addresses. These parameter addresses are offset from the base address by a fixed amount. By convention, the input parameters of a procedure are placed immediately after the base address, followed by the output procedures.

\begin{enumsubsub}

    \item
        There shall be one unique location on the server's AXIoE bus for each parameter in the abstract procedure signature.
    \item
        It shall be legal to place parameters at any location on the server's AXIoE bus, however the following convention is recommended.
        % NOTE: This is pretty much the system used in PMX.2
        \begin{enumsubsub}

            \item
                A \textit{base address} is provided as the execution address of the procedure. See Section~\ref{sub:translation-execution} for execution address rules of use.

            \item
                The first input parameter is offset one word from the base address on the server's AXIoE bus. Each input parameter appears offset one word from the previous input parameter. The offsets correspond to the order the parameters appear in the abstract procedure signature, read from left to right.

            \item
                The first output parameter is offset one word from the last input parameter. Each output parameter appears offset one word from the previous output parameter.

                \begin{example}
                    Consider the procedure \texttt{[out0] = EXAMPLE\_PROCEDURE(in0, in1)}. The concrete memory map for this procedure should appear as in Table~\ref{tab:axioe-concrete-signature-example}.



                \end{example}

        \end{enumsubsub}

    \item
        Parameters shall not be offset from one another by less than 4 bytes.

\end{enumsubsub}

% ---------------- Concrete Signature Example
\begin{table}
    \centering
    \caption{Example AXIoE concrete signature example}
    \label{tab:axioe-concrete-signature-example}
    \begin{tabular}{|l|l|l|}
        \hline
        \textbf{Parameter Name} & \textbf{Offset} & \textbf{Type} \\ \hline
        (Base Address) & 0 & Execution Parameter \\ \hline
        \texttt{in0} & +4 & Input value parameter \\ \hline
        \texttt{in1} & +8 & Input value parameter \\ \hline
        \texttt{out0} & +12 & Output value parameter \\ \hline
    \end{tabular}
\end{table}

% ---------------- RULES OF USE ---------------- %
%\subsection{Procedure Call Signature Rules of Use}
%\label{sub:signature-rules of-use}
%
%The following clauses define the rules of use for procedure call signatures.
%
%\begin{enumsub}
%
%    \item
%        Vendors should specify the abstract procedure signatures for any procedures that are implemented using AXIoE RPC.
%
%\end{enumsub}
%



