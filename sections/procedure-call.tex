% ---------------- PROCEDURE CALLS ---------------- %
\section{AXIoE Remote Procedure Calls}
\label{sec:axioe-remote-procedure-calls}

%This section describes the method and rules of use for decomposing a function prologue into a sequence of AXIoE transactions that can be used to initiate a procedure on a remote server. 

This section describes a method for executing procedure calls on a remote device using the AXIoE protocol. Procedure calls are decomposed into a sequence of AXIoE transactions according to the abstract and concrete procedure signatures. Grouping transactions in this way allows an AXIoE client to execute higher-level functions on an AXIoE server. 

% TODO: Need to do a much better job than this. Should read a printed copy of the PMX.2 comms section and review for this.


\subsection{Overview}
\label{sub:rpc-overview}

An AXIoE procedure has three components.

\begin{itemize}
    \item
        A procedure name.
    \item
        A set of input parameters.
    \item
        A set of output parameters.
\end{itemize}

Input parameters act as arguments to the procedure. Output parameters act as return values from the procedure. A procedure may have any number of input or output parameters. The function name acts as an entry point to the function.

Because the client and server do not share a common memory space the client must transmit the prologue of the procedure to the server. The format for this is specified by the procedure call signature (see section~\ref{sub:procedure-call-signatures}). 




% ---------------- PROCEDURE CALL SIGNATURES ---------------- %
\subsection{Procedure Call Signatures}
\label{sub:procedure-call-signatures}

%Procedures calls are defined by a \textit{procedure signature}. The procedure signature is intepreted in two ways.

Procedure calls are represented by a \textit{procedure signature}. This signature is interpreted in two ways.

\begin{itemize}

    \item
        An \textit{abstract} procedure signature implemented on the client side.
    \item
        A \textit{concrete} procedure signature implemented on the server side.

\end{itemize}

Abstract procedures are a notational device used to represent the procedure call in terms of a set of abstract inputs and outputs. The abstract procedure signature is implemented in any language and in any fashion by a client application. Abstract procedure signatures are represented using the following notation

\begin{codeexample}
    [out$0$, out$1$, \ldots out$N$] = PROCEDURE\_NAME(in$0$, in$1$, \ldots in$N$)
\end{codeexample}

where $in0$ through $inN$ represent the input parameters, $out0$ through $outN$ represent the output parameters, and \texttt{PROCEDURE\_NAME} is the name of the procedure. 

% TODO: Need a diagram to show the memory map for an example procedure signature.

Concrete procedure signatures are implemented on an AXIoE server. A concrete procedure signature relates parameters in the abstract procedure signature to physical or virtual addresses on the AXIoE bus. Each procedure has a base address, and one or more parameters addresses. These parameter addresses are offset from the base address by a fixed amount. By convention, the input parameters of a procedure are placed immediately after the base address, followed by the output procedures. 

Buffer parameter types are represented with a leading \texttt{*} mark. See section~\ref{subsub:procedure-parameter-types} for more information about buffer parameters.



% ---------------- PARAMETER TYPES ---------------- %
\subsubsection{Parameter Types}
\label{subsub:procedure-parameter-types}

Parameters are not strongly typed in AXIoE. In general parameters are considered to be 32-bit unsigned integers and are written into a 32-bit address space.

For procedure calls, there are two \textit{types} of parameters.

\begin{itemize}
    \item
        A value type.
    \item
        A buffer type.
\end{itemize}

Value types are implemented as a single read or write address on the AXIoE bus. An input parameter that is implemented as a value type accepts a single word written to the parameter address. An output parameter that is implemented as a value type allows a client to read a single word from the parameter address. A value type can be thought of as being similar to a \textit{register}.

Buffer types allow for multiple data words to be read from or written to a single address. A buffer type can be thought of as being similar to a \textit{FIFO}. 

The following clauses define the meaning and rules of use for procedure parameter types.

\begin{enumsubsub}

        % TODO : Buffer parameters shall be implemented such that multiple read transactions to the same address cause the internal 'buffer pointer' to increment by amount equal to the number of bytes requested in the read.
    \item
        Buffer parameters shall be written to and read from using the \texttt{FIXED} burst type. 

    \item
        Value parameters shall be written to and read from using the \texttt{INCR} type.

        % TODO : These two clauses should go into the transaction section
    \item
        \label{clause:client-explicit-data-size}
        Clients shall explicitly specify the amount of data to be transferred to an input parameter or from an output parameter. 

        % NOTE : These rules only really apply to procedure call parameters. Keep in mind when writing that procedure call parameters are a subset of more general calling parameters. The idea that buffer parameters are basically interfaces to stream pointers (think >> cin or cout <<) 
        \begin{enumsubsub}

            \item
                For a write request, servers shall accept the number of bytes of data specified by the client. 

            \item
                For a read request, servers shall provide the number of bytes requested by the client. If the client requests bytes from a buffer

        \end{enumsubsub}
        

    \item 
        Servers shall always provide the amount of data requested except where technical limitations (such as maximum packet size) prevent this. 

    \item
        If a client requests more data from a server than is available for  a particular transaction, the server shall pad the data with zeros to match the requested length. The padded data shall be appended to the furthest offset in the buffer output and shall be aligned on a 4-byte boundary.
        
        %For transactions where the amount of data is not well defined on the client side, the following clauses apply.

\end{enumsubsub}


% TODO : There should be somethig about the maximum size a buffer parameter can be in a particular implementation. In general buffer parameters should present themselves as having unlimited size, however in practice the documentation should provide upper limits to the amount of data that can be written to a buffer.


% ---------------- PARAMETER SIZES ----------------%
\subsubsection{Parameter Sizes}
\label{subsub:procedure-parameter-sizes}

% TODO : Short summarry - types are uint32 unless otherwise specified. Parameters may be 

All AXIoE parameters are required to align on a 4-byte boundary. 


%TODO : What to do about variable width parameters. 
\begin{enumsubsub}

    % TODO : Note that this HAS to be the case, since the AXIoE header and other transaction overhead data is 4-byte aligned,
    \item
        Procedure parameters shall always occupy 4-byte words. All parameters shall be aligned on a 4-byte boundary. 

    \item
        If a procedure accepts a parameter that is less than 4-bytes wide, that parameter shall be padded to align on a 4-byte boundary. 

    \item
        If a procedure accepts a parameter than is more than 4-bytes wide, that parameter shall be implemented as a \texttt{buffer} (see section~\ref{subsub:procedure-parameter-types}). The buffer shall accept the lowest 4-bytes first, followed by the next highest 4-bytes, and so on until the entire parameter has been written.

    %\item
    %    Procedure parameters shall always be word-aligned. Padding shall be inserted as required to ensure that all parameters fall on a word-aligned boundary.

    %\item
    %    The width of a word shall be fixed for a given procedure. 

\end{enumsubsub}


% ---------------- PASSING PARAMETERS ---------------- %
\subsubsection{Passing Parameters}
\label{subsub:procedure-passing-parameters}

Because the client and server do not share memory space all inputs to a procedure must be passed by value in a series of write transactions. Parameters that are normally passed by reference must be de-referenced and written to a buffer parameter during the formation of the procedure prologue. 




The following clauses define the rules for passing parameters to procedures in this specification.

\begin{enumsubsub}

    \item
        All parameters shall be passed explicitly by value. This shall occur as one or more write transactions to a parameter address associated with a procedure call.

    \item
        Input parameters that would normally be passed by reference shall be dereferenced before transmission and passed by value. The concrete signature of a procedure that accepts `reference' parameters shall provide a buffer to accept the dereferenced input.

    \item 
        Output parameters that would normally be passed by reference shall be dereferenced and placed into an output buffer. The client shall perform read transactions to the output parameter address to recover the data by value. 


        % NOTE : if a client performs a read transaction to a buffer parameter, that buffer parameter should provide the amount of data the client requested in all cases. If this data is less than the amount of data in the buffer, the read shall cause the internal buffer pointer to move ahead by the amount requested. Subsequent reads shall provide the amount of data requested in the read starting from the point where the most recent read finished. If the amount of data requested is more than the amount available, the server shall pad out the data with zeros to fit the requested length. 

        % NOTE: For a write transaction, the same logic applies. Multiple writes to a buffer parameter shall cause the 'internal pointer' to be advanced by a certain amount. This should keep happening until all data is witten. Ideally the buffer can be considered unlimited, but in practice this will not be the case. The server may wish to advertise the maximum amount of data that can be written to a buffer parameter in total (or perhaps more generally, the rules of use for writing to buffer parameters in a particular implementation). There may be total limits, limits within a certain amount of time, etc.

       % TODO : Some segment on the pattern for reading from a buffer in a procedure call. This pattern might be useful elsewhere but should only appear in this specification in this section. 



\end{enumsubsub}

% ---------------- PASSING LARGE AMOUNTS OF DATA ---------------- %
%\subsubsection{Passing Long Arrays of Data}
%\label{subsub:procedure-passing-long-arrays}
%
%Clients must specify the amount of data that is read from or written to a parameter address. In some cases a client may wish to transfer an amount of data that will not fit into a single packet. 
%
%\begin{enumerate}
%    \item
%        A client that performs a 
%
%\end{enumerate}


% ---------------- PROCEDURE CALL SEQUENCE ---------------- %
\subsection{Procedure Call Sequence}
\label{sub:procedure-procedure-call-sequence}
% TODO : This part may need to go after the sections explaining the components.

The following section describes the sequence of events that comprise an AXIoE procedure call.


\begin{enumsub}

    % TODO : Re-write this as a series of clauses?

    \item
        \textbf{Procedure Collection} - clients collect all data required in the prologue of the procedure. 

    \item
        \textbf{Procedure Translation} - The client creates a sequence of AXIoE read and write transactions according to the abstract signature of the procedure. Input parameters are executed first, followed by the entry point parameter, and finally by output parameters.

    \item
        \textbf{Transmission} - The client transmits the sequence of transactions to the server.

    \item
        \textbf{Execution} - The server executes the transactions in sequence. The transactions shall be sequenced such that write transactions to input parameters occur first, followed by read transactions to output parameters. 

    \item
        \textbf{Response} - The server transmits completions for all the transactions. These completions shall include the values of any output parameters requested by the client. 



\end{enumsub}




% ---------------- PROCEDURE TRANSLATION ---------------- %
\subsection{Procedure Translation}
\label{sub:procedure-procedure-translation}

% TODO : Do a diagram that has a concrete signature vertically on the right, and a stack of AXIoE transactions on the right (like the PMX.2 diagrams). The diagram has some arrows on the side.


Before a procedure can be executed on a remote server it must be translated into a sequence of AXIoE transactions. These transactions first build the function prologue in the remote server's memory space, perform a transaction to begin execution, and then read output parameter addresses to return execution outputs to the client.

% ---------------- PARAMETER TRANSLATION ---------------- % 
Figure~\ref{fig:axioe-proc-call-value} shows the translation sequence for an example procedure with the signature

\begin{codeexample}
    [out0, out1] = EXAMPLE\_PROCEDURE(in0, in1)
\end{codeexample}

This procedure call accepts two value parameters and returns two value parameters. A series of AXIoE transactions is made to each parameter address. The procedure prologue is formed by a series of write transactions to the input parameter addresses. A transaction is made to the base address to begin execution. In this example, the transaction is a write transaction that writes no data. The exact details of this transaction may vary for a given implementation (see Section~\ref{clause:procedure-base-address-execution}). Data is returned to the caller with a series of read transactions to the procedure's output parameter addresses. 

\begin{figure}
    \centering
    \includegraphics[width=\figwidthlg]{figures/rpc/axioe-proc-call-value}
    \caption{Illustration of an AXIoE procedure call with value parameters}
    \label{fig:axioe-proc-call-value}
\end{figure}
%The following section is for illustration purposes only, and does not form any part of the formal AXIoE RPC specification.

%\begin{itemize}
%    % TODO : The items in this list refer to the transactions/completions in the diagram.
%
%\end{itemize}

The following clauses define the behaviour for abstract signature translation.

\begin{enumsub}

    \item
        Each value input parameter that appears in a procedure's abstract signature shall be converted into an AXIoE write transaction. This write transaction shall write a single data word to the concrete address of the parameter. This write shall use the \texttt{INCR} burst type.

    \item
        Multiple writes to the same value parameter address shall overwrite the value for that parameter. The last write to a value parameter address shall be the value used during execution.

    \item
        Each buffer input parameter that appears in a procedure's abstract signature shall be converted into an AXIoE write transaction of arbitrary length. This type of write shall use the \texttt{FIXED} burst type. 

    \item
        Multiple writes to the same buffer parameter address shall cause new data to be appended to the buffer. 

    \item
        \label{clause:procedure-base-address-execution}
        A transaction to the procedure's base address shall cause the procedure to be executed. The specific form of the transaction shall be implementation specific. Vendors shall specify the transaction format that will cause procedure execution.

        \begin{example}
            Example: An AXIoE procedure call may be invoked by a read transaction to the base address or a write transaction to the base address. These shold be value transactions, but may be buffer transactions if required.
        \end{example}

    \item
        Each value output parameter that appears in a procedure's abstract signature shall be converted into an AXIoE read transaction. This read transaction shall read a single data word from the concrete address of the parameter. This read shall use the \texttt{INCR} burst type.

    \item
        \label{clause:procedure-mutiple-value-read}
        Multiple reads to the same value parameter address shall cause the same value to appear in the corresponding read completion except as per clause~\ref{clause:procedure-valid-bytes}.


    \item
        Each buffer output parameter that appears in a procedure's abstract signature shall be converted into an AXIoE read transaction of arbitrary length. This type of read shall use the \texttt{FIXED} burst type.

    \item
        Multiple reads to the same buffer parameter address shall cause additional data to be read from that address. For a series of read transactions, each transaction shall contain data that begins at the offset of the previous read transaction.

        \begin{example}
            Example: An output buffer parameter contains 2048 bytes of data to be read by a client application. The client performs a 256 byte AXIoE read transaction to the address of the output buffer parameter. The resulting read completion contains bytes 0 through 255. A subsequent read completion of the same size will contain bytes 256 through 511. This pattern continues until all 1024 bytes have been read.
        \end{example}

    \item
        Multiple reads to the same buffer parameter address shall always produce a read completion with the specified number of bytes. The server shall zero-pad the read completion if the number of bytes requested is greater than the number of bytes remaining in the buffer.

    \item
        \label{clause:procedure-valid-bytes}
        A procedure should, for each buffer output, provide a corresponding value output to indicate the number of valid bytes. For each read to a particular buffer output address, this value output shall indicate the number of bytes that contain valid data. 

    %\item
    %    \label{clause:procedure-valid-byte-offset}
    %   

        \begin{example}
            Example: \texttt{EXAMPLE\_PROCEDURE()} contains a buffer output parameter \texttt{*out\_param} which holds a total of 258 bytes. A client reads the buffer parameter in two transactions. In the first transaction, the client requests 256 bytes of data from \texttt{*out\_param}. The client then performs a read transaction to a value parameter \texttt{out\_valid} which holds the value 256, indicating that 256 of the 256 bytes contain valid data. The client then performs an additional read transaction to \texttt{*out\_param} requesting 256 bytes of data. A subsequent read to \texttt{out\_valid} returns the value 2, indicating that the first two bytes are valid, and the remaining 254 bytes are padding. Any further reads to \texttt{*out\_param} will return only padding bytes. Thus the value in \texttt{out\_valid} will always be equal to the number of requested bytes for every read transaction that occurs after the buffer has been fully read.
        \end{example}

\end{enumsub}




% ---------------- PROCEDURE EXECUTION ---------------- %
\subsection{Procedure Execution}
\label{sub:procedure-procedure-execution}

A procedure is executed by performing a transaction to it's base address. The type of transaction is specified in the procedures documentation. Output parameters are not returned automatically from a procedure. Instead a client must explicitly perform read transactions to the output parameter addresses. 

The following clauses define the rules of use for procedure execution.

\begin{enumsub}

    % TODO: This may need to go somewhere else
    \item
        A procedure shall consist of 
        \begin{enumsub}
            \item
                A set of input parameters, each of which is set by a write transaction to the parameter address.
            \item
                A set of output parameters, each of which is retreived by a read transaction to the parameter address.
            \item
                An entry point transaction which begins execution of the procedure. 
            \end{enumsub}

    \item
        \label{clause:procedure-defined-entry-point}
        A procedure shall have a defined entry point. This entry point may be either a read or write transaction, but shall be a specific operation that is performed in order for the body of the procedure to be executed. 

    \item
        % TODO : This needs some clarification in line with NRC feedback
        A server shall be free to delay execution of a procedure until a transaction is performed to the base address of the procedure. 

    \item
        A client shall explicitly perform a transaction to the base address of the procedure in order to begin execution. This may be a read or a write transaction.

    %\item
    %    A server shall be aware of its own memory map and shall therefore know the association between an address that is a parameter of a procedure and the procedure of which it is a part.

    \item
        It shall be legal for the value of a write transaction to the base address of a procedure to have some additional meaning beyond starting execution. This meaning, if present, is neither governed nor restricted by this specification. 

    \item
        A server should where possible provide default values for procedure arguments. See Section~\ref{sub:procedure-default-parameters} for more detail.

    \item
        After execution a client shall perform read requests to the output parameter addresses of a procedure to return any values to the caller.


\end{enumsub}





% ---------------- DEFAULT PARAMTERS ---------------- %
%\subsection{Default Parameters}
%\label{sub:procedure-default-parameters}
%
%In general there is no way to enforce that parameter transactions are performed in a particular order. Since clause~\ref{clause:procedure-defined-entry-point} mandates that a particular transaction shall trigger the execution of a procedure, it is possible for a client to perform the execution transaction before writing input parameter data. 
%
%
%% Server implementations are strongly encouraged to 
%
%
%\begin{enumsub}
%
%    \item
%        Implementors shall clearly document the parameter behaviour of procedures.
%
%
%\end{enumsub}
%

