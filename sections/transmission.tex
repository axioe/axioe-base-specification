% ---------------- TRANSMISSION ---------------- %
\section{Packet Transmission Protocol}
\label{sec:packet-transmission}

\subsection{Overview}
\label{sub:transmission-overview}

The specification has heavily described the server behaviour and lightly described the client behaviour. 
A real network will suffer from occasional errors such as a lost or corrupted packet.
The following sections describe the recommended error recovery mechanisms, however, a client implementation may use its own sequence of packets to recover from a network error.

The protocol described will recover from transitory network errors, guaranteeing that the encapsulated transactions arrive at the AXIoE Bus Master in the correct order and that their completions are returned to the client.

The protocol makes use of a sequence number to enumerate packets. If any packet is lost in the network between client and server the protocol will detect a skip in the sequence number. The client can then transmit a series of packets to determine the direction of the lost packet, and request a retransmission.

% ---------------- SLIDING WINDOW PROTOCOL ---------------- %
\subsection{Recommended Sliding Window Protocol}
\label{sub:transmission-sliding-window-protocol}

The sliding window mechanism uses the sequence number to identify and locate a packet in the order of transmission.
The width of the window describes how many packets can be in-flight. 
This allows the client to transmit a number of \pkt{REQ} packets in quick succession, before the first \pkt{CPL} packet is received. 
The full bandwidth of the network can therefore be utilised since the client does not need to wait for the \pkt{CPL} packet to be returned before sending a new \pkt{REQ} packet.

When deciding the window size at the client, the system designer should consider how many packets can be in-flight at once through the network, the probability of packet loss, and if the maximum bandwidth is needed. 
Typically each network element will completely buffer a packet (to calculate and check consistency) before transmitting it to the next network element. 

For example, in a simple system with a client and server connected by a switch, there will be 3 network elements between the application and AXIoE bus, and 3 network elements between the AXIoE bus and the application.
This means each element can be processing a different packet at once suggesting an optimal window size of 6.

The upper limit on the window size is determined by the server implementation.
This upper limit will usually be constrained by the memory resources available to the server and hence the size of the server's replay buffer.

If a \pkt{REQ} packet is lost, the server will see a jump in the sequence number and stop processing all other \pkt{REQ} packets. The client will need to retransmit the missing \pkt{REQ} packet and all subsequent \pkt{REQ} packets to recover. This is known as a \textit{go-back-N} behaviour.
In this case a larger window size will result in a larger good-put penalty in the event of a lost \pkt{REQ} packet.

The same penalty is not applied to lost \pkt{CPL} packets since the client can request a selective retransmission of a particular \pkt{CPL} packet.

An intelligent server could measure the optimal window size and dynamically adjust it to maximise good-put.

The following subsections describe in detail the use of the sliding window protocol for transmission error detection and recovery. The following notational conventions are used inside the diagrams contained in this section.

\begin{itemize}
    \item
        Each arrow crossing the diagram represents a single packet. Packets originating from clients are shown with solid lines. Packets originating from servers are shown with dashed lines.
    \item
        On the client side, two numbers are given above each packet in the form

        \begin{quote}
            \clientseq{A}{B}
        \end{quote}

        These represent the \textbf{Current Sequence Number} and \verb$(Expected Completion Number)$ respectively. Collectively they indicate the state of the client at a time \textit{immediately before} the arrow to which they are closest. 

    \item
        On the server side, a single number is given in the form

        \begin{quote}
            \serverseq{A}   
        \end{quote}

        This represents the \textbf{Expected Sequence Number}. This indicates the state of the server \textit{immediately after}  the arrow to which it is closest. 

    \item
        Figure~\ref{fig:transmission-axioe-probe-operation} demonstrates the use of this notation. 
        %Figure~\ref{fig:transmission-axioe-notation-example} demonstrates the use of this notation. 
        At the start of the diagram, the client sends a \pkt{PRB} packet. The state of the client is \clientseq{?}{-}. 

        \begin{itemize}
            \item
                The \textbf{?} indicates an unknown state\footnote{The term \textit{state} here can normally be considered as synonymous with \textit{sequence number}.} at that moment in time.
	    \item
                The dash in the form \verb$(-)$ indicates that the field is not applicable at that moment in time.
            
        \end{itemize}

\end{itemize}


%---------------- PROBE EXAMPLE ---------------- %
\subsubsection{\texttt{PRB} Example}
\label{subsub:transmission-prb-example}

The following section demonstrates the use of the \pkt{PRB} packet to determine the server state.

% ---------------- PROBE OPERATION FIGURE
\begin{figure}[ht!]
    \centering
    \scalebox{.85}{\input{tikz/axioe-probe-operation}}
    \caption{Example usage of the \pkt{PRB} packet.}
    \label{fig:transmission-axioe-probe-operation}
\end{figure}

\begin{itemize}
    \item
        At start of operation, the client is unaware of the servers sequence number. 
        This is represented using the notation \clientseq{?}{-}. 
        In this example, the server has an initial state \serverseq{X}. 
        In order to synchronise with the server, the client sends a \pkt{PRB}. 

    \item
        The server receives the \pkt{PRB} and replies with \pkt{PRB-X}, indicating that the server expects the next \pkt{REQ} packet to have sequence number \serverseq{X}.

    \item 
        The client sends \pkt{REQ} packets to the server starting with the sequence number returned in the previous \pkt{PRB} reply.
        In this example, the first \pkt{REQ} packet will be \pkt{REQ-X}. The subsequent packets will be \pkt{REQ-(X+1)}, \pkt{REQ-(X+2)} and so on.

    \item
        Operation continues as normal. See the following sections for examples of performing error recovery.

\end{itemize}


% TODO : The subsubsections are currently named after the pages in the original PDF file, and the text in the clauses is based on the text in the diagrams. 

% ---------------- NORMAL OPERATION ---------------- %
\subsubsection{Normal Operation}
\label{subsubs:tranmission-axioe-normal-operation}

Figure~\ref{fig:transmission-axioe-normal-operation} shows request and completion behaviour in the absence of network or packet errors. 

% TODO: Mention the use of the SOL bit here under normal operation (ie: no random restarts)

% ---------------- NORMAL OPERATION FIGURE
\begin{figure}[ht!]
    \centering
    \scalebox{.75}{\input{tikz/axioe-normal-operation}}
    \caption{Diagram of normal packet operation.}
    \label{fig:transmission-axioe-normal-operation}
\end{figure}

\begin{itemize}

    \item
        Prior to the start of the transfer, the client must synchronise its sequence number with the server as described in Section~\ref{subsub:transmission-prb-example}. 
    \item
        Once synchronised, the client can send \pkt{REQ-1} with the server's expected sequence number. 
    \item
        At this point, \pkt{REQ-1} should be appended to the client-side replay buffer. 
        Transmission of a \pkt{REQ} packet will start a time-out counter.
    \item
        Up to \texttt{MAX\_REPLAY\_BUFFER} packets may be in-flight at once. Therefore the client may send additional request packets before \pkt{CPL-1} arrives. 
    \item
        On the server side \pkt{REQ-1} arrives with the correct expected sequence number. 
        The server processes the request and sends back a completion with the matching sequence number (\pkt{CPL-1}). 
        The server increments its internal sequence counter. 
    \item
        In time, additional completions are received. 
        In this example all requests are received in order. 
        Therefore the server simply processes them in turn and replies with the corresponding completion sequence number, updating its internal sequence counter as it goes along. 
        The server sends \pkt{CPL-2}, and \pkt{CPL-3}.

        \begin{example}
            Packets which arrive out of order will rejected on the server side, requiring the client to re-transmit in order. See Section~\ref{subsub:transmision-lost-request-packet-recovery-by-nak} for an example of recovery from this type of error.
        \end{example}

    \item
        The client confirms that sequence number of the completion matches the sequence number of the requests. Once \pkt{CPL-0} has been received, the client can discard \pkt{REQ-0} by popping it from the client-side replay buffer.

\end{itemize}



%  ---------------- SOL BIT  ----------------  %
\subsubsection{\bit{SOL} Bit Recovery}
\label{subsub:transmission-sol-bit-recovery}

\begin{itemize}

    \item
        The client transmits \pkt{REQ-X} and receives \pkt{CPL-X}
    \item
        The server unexpectedly goes down. 
        In this example, the server's internal sequence counter is set to 0.
    \item
        The client transmits \pkt{REQ-X+1}. 
        Because the server is in the start-of-life state, the server does not respond to this request.
    \item
        The request times out. 
        The client has not seen a \pkt{CPL-X+1} or a \pkt{NAK-X+1}. 
    \item
        The client therefore attempts to recover the state of the server by sending a \pkt{PRB} packet.
    \item
        The server sends a \pkt{PRB} reply with the \bit{SOL} bit set, indicating that the server is in the start of life state.

        \begin{example}
            The client is now able to infer that the server has unexpectedly been reset. Application level logic in the client may need to be re-initialised.
        \end{example}

    \item
        The client re-synchronises and sends \pkt{REQ-0}. 


\end{itemize}

\begin{figure}[ht!]
    \centering
    \scalebox{.85}{\input{tikz/axioe-sol-operation}}
    \caption{Diagram showing recovery from unexpected server restart}
    \label{fig:transmission-axioe-sol-operation}
\end{figure}


% ---------------- NAK ERROR RECOVERY ---------------- %
\subsubsection{\pkt{NAK} Error Recovery Mechanism}
\label{subsub:transmission-nak-error-recovery-mechanism}

If a client determines a \pkt{CPL} has been lost, the client may request the \pkt{CPL} packet to be replayed from the server's replay buffer. 
The client may request a packet up to \texttt{MAX\_REPLAY\_BUFFER} packets old. The following section describes the mechanism for replaying a packet from the replay buffer.

If the server receives a \pkt{REQ} packet that does not have the expected sequence number (because a \pkt{REQ} packet was lost) then it will inform the client with a \pkt{NAK} packet. 

% ---------------- NAK ERROR RECOVERY FIGURE
\begin{figure}[ht!]
    \centering
    \scalebox{.85}{\input{tikz/axioe-nak-operation}}
    \caption{Diagram of \pkt{NAK} error recovery mechanism}
    \label{fig:transmission-axioe-nak-error-recovery-mechanism}
\end{figure}

% TODO : Numbering of components in the diagram?
\begin{itemize}

    \item
        To retrieve a completion from the replay buffer of the server, the client sends a \pkt{NAK} packet to the server containing the sequence number of the packet to be replayed. 

        \begin{example}
            In Figure~\ref{fig:transmission-axioe-nak-error-recovery-mechanism}, the client attempts to replay packet $X$. The client is expecting completion $X$ to arrive in response.
        \end{example}

    \item
        Upon receipt of a \pkt{NAK} packet, the server shall inspect the sequence number. The server shall reply to the incoming \pkt{NAK} with the \pkt{CPL} packet matching this sequence number. Note that the client may not ask for \pkt{CPL} packet unless it knows it is in the replay buffer, see clause~\ref{clause:client-no-replay}.

        \begin{example}
            In this example, the replay buffer on the server does contain the requested completion. See Section~\ref{subsub:transmission-lost-completion-packet-recovery} for an example of recovering from a lost completion packet.
        \end{example}

    \item
        The server shall not process any \pkt{REQ} packet received out of order. The server shall respond to all out of order \pkt{REQ} packets with \pkt{NAK} packets. The sequence number of the \pkt{NAK} packet shall be the server's expected sequence number.

    \item
        When the client receives the \pkt{NAK-(X+1)} packet, the client can interpret this to mean the server is expecting a \pkt{REQ-(X+1)} packet, and therefore did not receive the previously transmitted \pkt{REQ-(X+1)} packet. 

\end{itemize}

% ---------------- LOST COMPLETION PACKET RECOVERY ---------------- %
\subsubsection{Lost Completion Packet Recovery}
\label{subsub:transmission-lost-completion-packet-recovery}

In this example, we walk though the recovery of a lost \pkt{CPL} packet, as illustrated in Figure~\ref{fig:transmission-axioe-lost-completion-packet-recovery}. 

% ---------------- LOST CPL RECOVERY FIGURE
\begin{figure}[ht!]
    \centering
    \scalebox{.85}{\input{tikz/axioe-lost-completion-packet-recovery}}
    \caption{Diagram showing recovery from a lost \pkt{CPL} packet}
    \label{fig:transmission-axioe-lost-completion-packet-recovery}
\end{figure}

% Example walkthrough of NAK 
\begin{itemize}

    \item
	We start with the client knowing the correct sequence number, either through the use of a \pkt{PRB} packet as in section~\ref{subsub:transmission-prb-example}, or though previous communication.
    \item
        The client sends a series of \pkt{REQ} packets. The client is able to ramp up packet transmission until the maximum allowable number of in-flight packets has been transmitted.\footnote{In this case, 3.}
    \item
        The server receives all packets and executes the required transactions, and transmits the corresponding \pkt{CPL} packets. 
        However, \pkt{CPL-0} is lost in transmission. 
    \item
        The client notices that the first completion returned is out of order.
        Since \pkt{CPL-1} has been returned, the client knows that the server must have executed \pkt{REQ-0} and created \pkt{CPL-0}.
        \pkt{CPL-0} will be in the server's replay buffer.
    \item
        The client sends a \pkt{NAK-0} to request a selective retransmission of \pkt{CPL-0} so that it can release the completion transactions of \pkt{CPL-0},\pkt{CPL-1}, and \pkt{CPL-2} in order back to the application.
    \item
	Now that \pkt{CPL-0} has been recovered, it is no longer `in-flight', so the sliding window can advance through \pkt{CPL-1} and \pkt{CPL-2}. Further \pkt{REQ} packets blocked by the sliding window can now be transmitted.        
\end{itemize}

% ---------------- LOST REQUEST PACKET RECOVERY ---------------- %
\subsubsection{Lost Request Packet Recovery by \pkt{NAK}}
\label{subsub:transmision-lost-request-packet-recovery-by-nak}

In this example, we walk though the recovery of a lost \pkt{REQ} packet, as illustrated in Figure~\ref{fig:transmission-axioe-lost-request-packet-recovery-by-nak}.

% ---------------- LOST REQ PACKET RECOVERY FIGURE
\begin{figure}[ht!]
    \centering
    \scalebox{.85}{\input{tikz/axioe-lost-request-packet-recovery-by-nak}}
    \caption{Diagram showing recovery of lost \pkt{REQ} packet by \pkt{NAK}};
    \label{fig:transmission-axioe-lost-request-packet-recovery-by-nak}
\end{figure}

\begin{itemize}

    \item
    We start with the client knowing the correct sequence number, either through the use of a \pkt{PRB} packet as in section~\ref{subsub:transmission-prb-example}, or though previous communication.
    \item
	The client begins by sending \pkt{REQ-0}.
	
    \item
        Additional requests are sent in packets \pkt{REQ-1} and \pkt{REQ-2}. \pkt{REQ-1} is lost due to a network error.
        
    \item
        The server receives and processes \pkt{REQ-0} and returns \pkt{CPL-0}. 
        
    \item
        Now that \pkt{CPL-0} has arrived, the sliding window has advanced forward by one packet and a new slot is available. The client sends \pkt{REQ-2} and awaits \pkt{CPL-1}.

    \item
        The server receives \pkt{REQ-2}, however it has not yet seen \pkt{REQ-1}. The server rejects \pkt{REQ-2} as being out of order and replies with \pkt{NAK-1}, indicating that it is expecting \pkt{REQ-1} next. The server does not process any request transactions in \pkt{REQ-2}.

    \item
        The client receives \pkt{NAK-1} from the server. This implies that \pkt{REQ-1} has either gone missing.
	The client transmits a \pkt{PRB} packet to act as a bookmark, allowing other \pkt{NAK-1} packets to arrive and avoid triggering the error recovery again. 
        
    \item
        The \pkt{REQ-3} arrives at the server. 
        Because this request is also out of order the server rejects it and does not process the transactions, and replies with \pkt{NAK-1} again. 
        
    \item
	The bookmark \pkt{PRB} packet arrives at the server, to which the server replies with with a \pkt{PRB-1} packet, confirming the server's expected sequence number.
	
    \item
        The \pkt{PRB-1} packet arrives at the client. The sequence number of the \pkt{PRB} reply is 1, confirming that \pkt{REQ-1} went missing.
        The client has ignored all additional \pkt{NAK-1} packets, and the network has now been flushed of all packets. The error recovery can now continue. 
        
    \item 
	The client resends \pkt{REQ-1},\pkt{REQ-2}, and \pkt{REQ-3} from its replay buffer, to refill the sliding window. 

    \item
        The server receives \pkt{REQ-1}, processes the transactions, and returns \pkt{CPL-1}. The server expected sequence number advances and operation resumes as normal.

\end{itemize}

\subsection{Lost Packet Recovery by Time-out}
\label{sub:transmission-lost-packet-recovery-by-timeout}

The previous sections have had the loss of a packet reported to the client either by a \pkt{NAK} packet or an out of order \pkt{CPL} packet, sent by the server in response to subsequent \pkt{REQ} packets.
If however, no subsequent \pkt{REQ} packet has been sent, then a lost \pkt{REQ} or \pkt{CPL} packet will go unnoticed and the application will hang.

The client maintains a time-out for each packet it sends. If that time-out expires then the client takes action to recover the presumably lost packet.

\subsubsection{Timed-out \pkt{PRB} packet}
\label{subsub:transmission-timed-out-prb-packet}

If a \pkt{PRB} packet times-out, then the client resends the \pkt{PRB} packet, restarting the time-out.
This is safe, since the \pkt{PRB} does not change the server state.

\subsubsection{Timed-out \pkt{NAK} packet}
\label{subsub:transmission-timed-out-nak-packet}
If a \pkt{NAK} packet times-out, then the client resends the \pkt{NAK} packet, restarting the time-out. 
This will recover a lost \pkt{NAK} or its corresponding \pkt{CPL}.
This is safe, since the client has already determined that the \pkt{CPL} packet exists in the server replay buffer. 
Re-requesting the \pkt{CPL} does not change the server state. 
If the client subsequently receives the original \pkt{CPL} packet then it can safely ignore the second copy of the \pkt{CPL} packet.

\subsubsection{Timed-out \pkt{REQ} packet}
\label{subsub:transmission-timed-out-req-packet}

If a \pkt{REQ} packet times-out, then the client must first determine if the \pkt{REQ} packet was lost, or the corresponding \pkt{CPL} was lost.
It can do this by sending a \pkt{PRB} packet to check the server's expected sequence number, as returned by the \pkt{PRB-N} reply packet.

  \begin{itemize}
    \item 
	If the expected sequence number is the same as the timed-out \pkt{REQ} packet then the \pkt{REQ} packet must have been lost.
	The client should retransmit the \pkt{REQ} packet and any subsequent \pkt{REQ} packets.

    \item 
	If the expected sequence number is different to (ahead of) the timed-out \pkt{REQ} then the \pkt{REQ} packet must have been processed by the server and the \pkt{CPL} packet lost.
	The client should send a \pkt{NAK} to selectively recover the lost \pkt{CPL} packet.

\end{itemize}

% TODO : Legalese version of this?
\subsection{Best Effort Delivery Protocol}
\label{sub:transmission-best-effort-delivery-protocol}

The following clauses define the rules of use for Best Effort Delivery.

\begin{itemize}

    \item
        Setting the \texttt{IGN} but in the \pkt{REQ} packet shall cause the sequence number check on the server side to be disabled.

    \item
        Disabling the sequence number check shall disable the storage of \pkt{CPL} packets in the server's replay buffer.

        \begin{example}
            Note that disabling the sequence number check will effectively reduce AXIoE to the underlying UDP protocol.
        \end{example}


\end{itemize}

%The sequence number check at the server can be disabled by setting the \texttt{IGN} bit in the \pkt{REQ} packet. This also disables the storage of \pkt{CPL} packets in the server's replay buffer.
%
%In effect the protocol is reduced to the underlying UDP protocol.



