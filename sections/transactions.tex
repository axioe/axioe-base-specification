% ---------------- TRANSACTIONS ---------------- %
\section{Transaction Structure}
\label{sec:transaction-structure}

The following section describes the structure of an AXIoE transaction.

\subsection{Overview}
\label{sub:transaction-overview}

A transaction is an atomic unit of communication that encapsulates a single read or write operation. A transaction may be made to a single memory location, a contiguous block of memory locations, or a write-in-order or read-in-order buffer that is exposed through a single memory location. 

In general, a transaction in AXIoE consists of
\begin{itemize}
    \item
        A 32-bit transaction header.
    \item
        A 32-bit address.
    \item
        Zero or more 32-bit data words.
\end{itemize}
All fields are big-endian.

A complete AXIoE transaction consists of a \textit{request} issued by an AXIoE client, and a \textit{completion} issued by an AXIoE server in response to the request. Request transactions only originate from clients. A completion transaction is constructed by the server after executing the request transaction. Completion transaction headers include status information about execution. There is always a one-to-one correspondence between request transactions and  completion transactions.

% ---------------- TRANSACTION COMPONENTS ---------------- %
\subsection{Transaction Components}
\label{sub:transaction-components}

The following sections define the components of a transaction request as well as the rules of use for each component. 

\begin{figure}[ht]
    \centering
    \scalebox{.75}{\input{tikz/axioe-complete-transaction-generic}}
    \caption{Diagram of a generic AXIoE transaction}
    \label{fig:axioe-complete-transaction-generic}
\end{figure}

\subsubsection{Transaction Header}
\label{subsub:transaction-header}

All AXIoE transactions begin with a transaction header which provides information about a particular transaction. 
The layout for the header frame is given in figure~\ref{fig:axioe-transaction-header}.
The meaning and use of each field in the transaction header is given in table~\ref{tab:axioe-transaction-header}.

When executing a transaction, the AXIoE Bus Master shall generate a response indicating the success of the transaction. 
This response is included in the completion transaction header.

%The \texttt{COND} bit may be set in the transaction header. Doing so instructs the server to execute the transaction only if the previous transaction was executed successfully.

% ---- Transaction Header Figure
\begin{figure}[ht!]
    \centering
    \scalebox{.95}{\input{tikz/axioe-transaction-header-generic}}
    \caption{Diagram of an AXIoE transaction header}
    \label{fig:axioe-transaction-header}
\end{figure}


% ---- Transaction header table
\input{tables/axioe-transaction-header}

% ---- Burst type table
\input{tables/axioe-burst}

\begin{enumsubsub}

    % Definitions of beats and bursts lifted from AMBA-AXI4
    \item
        A \textit{beat} shall refer to a unit of data that is transferred to the AXIoE bus. The number of bytes in a beat is indicated by the \field{SIZE} field, see Table~\ref{tab:axioe-transaction-header}.

    \item
        A \textit{burst} shall refer to a series of \textit{beats} transferred to the AXIoE bus in a single transaction. The number of \textit{beats} contained within the transaction \textit{burst} is indicated by the \field{LENGTH} field, see Table~\ref{tab:axioe-transaction-header}.

    \item
        \label{clause:transaction-burst-length-restriction}
        The burst length may be restricted by implementation considerations. The burst length shall not exceed 2047 or the maximum allowable length for a specific AXIoE bus implementation, whichever is lowest.

        \begin{example}
            If the AXIoE Bus is implemented as an AMBA-AXI4 Bus, this will limit the burst length to within the range 0 through 255. See~\cite{axi4-spec} for more information.
        \end{example}

    \item
        The total number of bytes in a transaction shall be given by
        \begin{equation}
            B_t = (\texttt{LENGTH}+1) \times 2^{\texttt{SIZE}},
        \end{equation}
        where \field{LENGTH} and \field{SIZE} are defined in table~\ref{tab:axioe-transaction-header}.

        \begin{enumsubsub}

            \item
                \label{clause:transaction-read-completion-data-bytes}
                For a read completion, $B_t$ shall represent the total number of data bytes in associated with that completion.
            \item
                \label{clause:transaction-write-request-data-bytes}
                For a write request, $B_t$ shall represent the total number of data bytes to be written in the transaction.
            \item
                \label{clause:transaction-read-request-data-bytes}
                For a read request, $B_t$ shall represent the number of bytes to be read in the transaction. 
            \item
                \label{clause:transaction-write-completion-data-bytes}
                For a write completion, $B_t$ shall represent the number of data bytes written. There shall be no data bytes in a write completion.

        \end{enumsubsub}

        \item
            Clients shall set the \field{RESP} field in the request transaction header to the value \field{0b0000}.
        \item
            Servers shall set the \field{RESP} field in the completion transaction header according to the description in table~\ref{tab:axioe-response}.
        \item
            All other \field{RESP} values are reserved.
        \item
            Setting the \bit{COND} bit shall cause the transaction to be executed only if the previous transaction returned an \enum{OKAY} status in its response field. See clause~\ref{clause:transaction-cond-preverr}.
	%    \begin{enumsubsub}
	%	\item
	%	    The first transaction received by the server shall presume a previous transaction response of \enum{OKAY}.
	%	\item
	%	    The previous transaction response shall persist between packets. See Table~\ref{tab:axioe-response} for for information about transaction responses.
	%    \end{enumsubsub}
        %\begin{enumsubsub}
        %    \item
        %        If the \texttt{COND} bit is set in the current transaction and the previous transaction did not return \texttt{OKAY} status in it's response field, the status of the current transaction shall be \texttt{PREVERR}. See Table~\ref{tab:axioe-response}.
        %\end{enumsubsub}

\end{enumsubsub}

% ---------------- RESPONSE CODES
\input{tables/axioe-response}

% ---------------- TRANSACTION TYPES ---------------- %
% TODO: Explain using a table?  
% TODO: Move this down
\subsubsection{Transaction Types}
\label{sub:transaction-types}

There are 4 types of transaction - read request, write request, read completion, and write completion. Table~\ref{tab:axioe-transaction-types} lists the combinations of the \bit{WR} and \bit{CPL} bits that specific each of these transaction types.

% ---------------- CPL and WR combination table ---------------- %
\input{tables/axioe-transaction-types}

% ---------------- TRANSACTION ADDRESSES ---------------- %
\subsubsection{Transaction Addresses}
\label{subsub:transaction-transaction-addresses}

\begin{enumsubsub}

    \item
        The address field shall be a 32-bit byte address. 
    \item
        The address shall be aligned to $2^{\texttt{SIZE}}$ bytes.

        \begin{example}
            Example: If \texttt{SIZE} = 2, then each data beat is 4 bytes (32 bits). 
            The address must be aligned to a 4 byte boundary, implying that the two least significant bits of the address will be zero.
        \end{example} 

    \item
        The address shall refer to the \textit{first} data beat in the transaction data block.  

        \begin{example}
            The \textit{first} word is shown as $a+0$ in figure~\ref{fig:transaction-axioe-transmission-big-endian}. Data elements are read out in the order shown. 
        \end{example}


\end{enumsubsub}

% ---------------- ENDIANNESS DIAGRAM
\begin{figure}[ht!]
    \centering
    \scalebox{.95}{\input{tikz/axioe-transmission-big-endian}}  % TODO : Rename this file
    \caption{Endianness of data in AXIoE packet payload}
    \label{fig:transaction-axioe-transmission-big-endian}
\end{figure}

% ----------------TRANSACTION DATA ----------------% 
\subsubsection{Transaction Data}
\label{subsub:transaction-transaction-data}

\begin{enumsubsub}

    \item 
	Data beats shall be encoded in big-endian format (network byte order, most significant byte first).
    \item
        Data bursts shall be transmitted in incrementing address order (network byte order).
    \item
        Data bursts shall be padded to a multiple of 4 bytes.

\end{enumsubsub}


% ---------------- TRANSACTION COMPLETIONS ---------------- %
\subsection{Completion Transactions}
\label{sub:transaction-completions}

Individual transactions are considered to be complete when their corresponding completion is received. A completion is a transaction which has the \texttt{CPL} bit set in its transaction header. As is the case with requests, completions may be concatenated into a single return packet. The following clauses apply to AXIoE completions.

\begin{enumsub}

    \item
        A completion shall begin with an AXIoE transaction header in which the \bit{CPL} bit it set, followed by the AXIoE Bus address to which the original request was sent.
    \item
        The completion transaction header will copy the \bit{WR}, \bit{COND}, \bit{SIZE}, and \bit{LENGTH} fields from the request transaction header, and copy the transaction address.
    \item
        A read completion shall include all data words that are associated with the original read request.
    \item \label{clause:read-data-sub}
        If the AXIoE Bus Master cannot return all the requested data, then the server shall pad the returned data such that the total length is equal to the requested length.
    \item
        A write completion transaction shall not contain any data words.
    \item
        The server shall generate a completion transaction for each request transaction it receives.
        \begin{enumsub}
            \item
                The server shall place one of the response codes given in table~\ref{tab:axioe-response} into the header of the completion transaction. 
            \item
                \label{clause:transaction-cond-preverr}
                If the request transaction is skipped because the \bit{COND} bit is set and the previous transaction returned an error, the the server shall place the \enum{PREVERR} response code in the \texttt{RESP} field of the completion transaction.
                Note, if it is a read transaction, then the server must substitute the data as per clause \ref{clause:read-data-sub}.
        \end{enumsub}
    \item
        \label{clause:transaction-order-of-completions}
        The order of completions in the response packet shall match the order of transactions in the request packet.

\end{enumsub}


% ---------------- COND bit ---------------- %
\subsection{Conditional Transaction Execution (\bit{COND})}
\label{sub:cond-transaction}
\begin{enumsub}
    \item
        If the \bit{IGN} bit is set in the packet global header (see section~\ref{subsub:packet-axioe-global-header}) then the \bit{COND} bit shall be ignored for all contained transactions, and the previous transaction error state shall be maintained and not updated for any transaction in this packet.
    \item
        If the \bit{COND} bit is set the transaction shall be executed only if the \enum{OKAY} response was received for the previous transaction.
        \begin{enumsubsub}
        \item
            The first transaction received by the Server shall presume a previous transaction response of \enum{OKAY}.
        \item
            The previous transaction response shall persist between packets. See Table~\ref{tab:axioe-response} for for information about transaction responses.
        \end{enumsubsub}

\end{enumsub}

