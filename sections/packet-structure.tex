% ---------------- PACKET STRUCTURE ---------------- %
\section{Packet Payload Structure}
\label{sec:packet-structure}

\subsection{Overview}
\label{sub:packet-overview}


This specification defines a packet format to provide reliable in-order transport of AXIoE Bus Transactions, from a Client to a Server over a network.
The packet format is intended to be operated in an Ethernet/IP/UDP protocol stack, however the AXIoE Bus Transaction payload may be delivered over any unreliable bi-directional packet-switched network. An AXIoE packet consists of,

\begin{enumsub}
    \item
        Any required Ethernet, IP, and UDP frame information.
    \item
        An AXIoE packet global header containing the AXIoE global fields.
    \item
        A payload of zero or more 32-bit words, depending on the packet type.
\end{enumsub}


% ---------------- PACKET CONTENTS ---------------- %
\subsection{Packet Contents}
\label{sub:packet-packet-contents}

An AXIoE packet contains a 4 byte global header that specifies the protocol version, packet type, and status or control of the sliding window algorithm. 
A sliding window algorithm is used to ensure reliable and in-order delivery of the transactions. Two packet types contain one or more transactions following the global header.
Figure~\ref{fig:axioe-packet-med} illustrates a packet containing two transactions.  

\begin{figure}[ht!]
    \centering
    \scalebox{.99}{\input{tikz/axioe-write-and-read}}
    \caption{Structure of an AXIoE request packet with two concatenated transactions; a write transaction with two data words, and a read transaction.}
    \label{fig:axioe-packet-med}
\end{figure}

% ---------------- AXIOE GLOBAL FIELDS ---------------- %
\subsubsection{AXIoE Global Header}
\label{subsub:packet-axioe-global-header}

% ---------------- GENERIC PACKET HEADER 
\begin{figure}[ht!]
    \centering
    \scalebox{.99}{\input{tikz/axioe-packet-header-generic}}
    \caption{AXIoE packet header fields.}
    \label{fig:axioe-packet-header}
\end{figure}

The global header describes the packet type and sliding window status for in-order delivery and error recovery. Table~\ref{tab:axioe-packet-global-fields} lists the meaning of each global header field.

\input{tables/axioe-packet-global-fields}

% ---------------- START OF LIFE

The \bit{SOL} bit (Start of Life) is set by the Server to indicate that it has just initialised, either as the result of a power on, or an unexpected reset.
When the Client receives a packet with this bit set, the Client cannot rely on any state or memory at the Server, even if they were in the middle of a conversation.
The Server will not process any requests until the start of life state is acknowledged by the Client.
This avoids executing transactions on a device of unknown state.
The start of life state is cleared at the Server by a packet sent from the Client where the \bit{SOL} bit is set.

The \pkt{SOL} mnemonic is overloaded depending on the direction of transmission.
\begin{itemize}
    \item
        In a packet sent from the sever, the \bit{SOL} bit is referred to as the \bit{SOL\_SET} bit.
    \item
        In a packet sent from the client, the \bit{SOL} bit is referred to as the \bit{SOL\_ACK} bit.
\end{itemize}
Use of these terms is encouraged to disambiguate Server and Client interactions.
See section \ref{subsub:server-start-of-life} for the Server's use of the \bit{SOL} bit.
See section \ref{subsub:client-sol-bit} for the Client's use of the \bit{SOL} bit.

% ---------------- PACKET TYPES ---------------- %
% TODO : Editors note - these should be nouns rather than verbs
\subsection{Packet Types}
\label{sub:packet-packet-types}

AXIoE defines 6 packet types, 3 for each direction. Packet types are indicated by setting the relevant bits in the AXIoE packet header. See Table~\ref{tab:axioe-packet-types} for a summary of the packet types and their field encoding.

\input{tables/axioe-packet-types}

% ---------------- REQ
\subsubsection{\pkt{REQ} Packet}
\label{subsub:packet-req-packet}

\pkt{REQ} packets are Client originated packets that contain request transactions. See Figure~\ref{fig:axioe-packet-header-req}.

\begin{enumsubsub}

    \item
        A \pkt{REQ} packet shall consist of a global header with the \bit{REQ} bit set. The \bit{NAK} and \bit{PRB} bits shall be cleared.

    \item
        A \pkt{REQ} packet shall contain one or more request transactions immediately following the global header.

\end{enumsubsub}

% ---------------- REQ HEADER
\begin{figure}[ht!]
    \centering
    \scalebox{.95}{\input{tikz/axioe-packet-header-req}}
    \caption{\pkt{REQ} packet header fields.}
    \label{fig:axioe-packet-header-req}
\end{figure}


% ---------------- CPL
\subsubsection{\pkt{CPL} Packet}
\label{subsub:packet-cpl-packet}

\pkt{CPL} packets are server originated packets that contain completion transactions. See Figure~\ref{fig:axioe-packet-header-cpl}.

\begin{enumsubsub}

    \item
        A \pkt{CPL} packet shall consist of a header with the \bit{REQ}, \bit{NAK}, and \bit{PRB} bits cleared.
        
    \item
        A \pkt{CPL} packet shall contain a completion transaction for each request transaction in the corresponding \pkt{REQ} packet. 


\end{enumsubsub}

% ---------------- CPL HEADER 
\begin{figure}[ht!]
    \centering
    \scalebox{.95}{\input{tikz/axioe-packet-header-cpl}}
    \caption{\pkt{CPL} packet header fields.}
    \label{fig:axioe-packet-header-cpl}
\end{figure}


% ---------------- PRB
\subsubsection{\pkt{PRB} Query Packet}
\label{subsub:packet-prb-query-packet}

A client may send a \pkt{PRB} packet to query the server for
\begin{itemize}
 \item the server's expected sequence number, and
 \item the server capabilities.
\end{itemize}
The server will respond with a \pkt{PRB} reply packet.
%Figure~\ref{fig:axioe-packet-header-prb-query}.

\begin{enumsubsub}

    \item
        The global header shall have the \bit{REQ} and \bit{PRB} bits set. The \bit{NAK}, and \bit{IGN} bits shall be cleared.
    \item
        A \pkt{PRB} packet shall contain no transactions. 

\end{enumsubsub}

% ---------------- PRB PACKET
\begin{figure}[ht!]
    \centering
    \scalebox{.95}{\input{tikz/axioe-packet-header-prb-query}}
    \caption{\pkt{PRB}-query packet header fields.}
    \label{fig:axioe-packet-header-prb-query}
\end{figure}

% ---------------- PRB
\subsubsection{\pkt{PRB} Reply Packet}
\label{subsub:packet-prb-reply-packet}

The \pkt{PRB} reply packet is transmitted by the Server in response to a probe query packet. It contains the server's expected sequence number and the Server capabilities structure.

\begin{enumsubsub}

    \item
        The global header shall have the \bit{PRB} bit set. The \bit{REQ}, \bit{NAK}, and \bit{IGN} bits shall be cleared.
    \item
        A \pkt{PRB} reply packet shall contain no transactions.
    \item
        A \pkt{PRB} reply packet shall be generated by the Server for each \pkt{PRB} request packet. Following the global header, the \pkt{PRB} reply shall contain a body as specified in Section~\ref{subsub:packet-prb-payload}.
    \item
        The sever shall set the sequence number of the \pkt{PRB} reply packet to the expected sequence number of the Server.

\end{enumsubsub}

% ---------------- PRB PACKET
\begin{figure}[ht!]
    \centering
    \scalebox{.95}{\input{tikz/axioe-packet-header-prb}}
    \caption{\pkt{PRB}-reply packet header fields.}
    \label{fig:axioe-packet-header-prb-reply}
\end{figure}

% ---------------- PRB PAYLOAD ---------------- %
\subsubsection{\pkt{PRB} Packet Payload - Server Capabilities}
\label{subsub:packet-prb-payload}

The \pkt{PRB} reply packet returns information about the configuration of a Server.
Figure~\ref{fig:axioe-prb-payload} shows the layout of a \pkt{PRB} payload and table~\ref{tab:axioe-prb-payload-tab} describes the bit fields.
The following clauses define the rules of use for the \pkt{PRB} payload.

\begin{figure}[ht!]
    \centering
    \scalebox{.95}{\input{tikz/axioe-prb-payload}}
    \caption{\pkt{PRB} packet payload fields.}
    \label{fig:axioe-prb-payload}
\end{figure}

\begin{enumsubsub}
    \item
        The Server shall support one or more burst types. Support shall be indicated as per Table~\ref{tab:axioe-prb-burst-type}.
    \item
        The Server shall support one or more beat sizes. Support shall be indicated as per Table~\ref{tab:axioe-prb-size}.
    \item
        The \bit{VENDOR\_ID} and \bit{DEVICE\_ID} fields shall be according to Section~\ref{sec:identification}. % TODO : Section reference for vendor and device
    \item
        The \bit{AOI} should point to the reccomended entry point address. 
        The contents located at the \bit{AOI} address shall be implied by the values of the \bit{VENDOR\_ID} and \bit{DEVICE\_ID} fields.

        \begin{example}
            An example entry point might be the base address of a Self-Describing Bus \cite{sdb-spec} table, or a proprietary configuration description file.
        \end{example}

    \item
        The \bit{AOI} address \texttt{0xFFFFFFFF} shall mean that there is no address of interest.
    % TODO: VENDOR 0 is the null vendor, DEVICE 0 is the null device

\end{enumsubsub}

% Overview of fields in PRB payload
\input{tables/axioe-prb-payload-tab}

% Table of burst types
\input{tables/axioe-prb-burst-type}

% Table of sizes
\input{tables/axioe-prb-size}


% ---------------- NAK
\subsubsection{\pkt{NAK} replay packet}
\label{subsub:packet-nak-packet}

A \pkt{NAK} replay packet is transmitted by a Client to request the retransmission (replay a copy) of the \pkt{CPL} packet with the matching sequence number from replay buffer at the server. See Section~\ref{subsub:server-replay-buffer} for further information about retransmitting a \pkt{CPL} packet.

\begin{enumsubsub}
    
    \item 
        The \bit{REQ} and \bit{NAK} bit shall be set. The \bit{PRB}, and \bit{IGN} bits shall be cleared.
    \item
        The \field{SEQ} field shall be set to the sequence number of the \pkt{CPL} packet the server should retransmit.
    \item
        A \pkt{NAK} replay packet shall contain no transactions. See Section~\ref{sub:packet-packet-types}.
\end{enumsubsub}

% ---------------- NAK PACKET 
\begin{figure}[ht!]
    \centering
    \scalebox{.95}{\input{tikz/axioe-packet-header-nak-replay}}
    \caption{\texttt{NAK}-replay packet header fields.}
    \label{fig:axioe-packet-header-nak-replay}
\end{figure}
\subsubsection{\pkt{NAK} packet}
\label{subsub:packet-nak-packet-replay}

A \pkt{NAK} packet provides a negative acknowledgement for a \pkt{REQ} packet. It is transmitted by a Server when the sequence number of a \pkt{REQ} packet did not match the Server's expected sequence number.

\begin{enumsubsub}

    \item
        The \bit{NAK} bit shall be set. The \bit{REQ}, \bit{PRB}, and \bit{IGN} bits shall be cleared.
    \item
        The \field{SEQ} field shall be set to sequence number that the server expected to receive.
    \item
        A \pkt{NAK} packet shall contain no transactions. See Section~\ref{sub:packet-packet-types}.

\end{enumsubsub}

% ---------------- NAK PACKET
\begin{figure}[ht!]
    \centering
    \scalebox{.95}{\input{tikz/axioe-packet-header-nak}}
    \caption{\texttt{NAK} packet header fields.}
    \label{fig:axioe-packet-header-nak}
\end{figure}

% ---------------- TRANSACTION CONCATENATION ---------------- %
\subsection{Transaction Concatenation}
\label{sub:transaction-concatenation}

To achieve high throughput, transactions may be concatenated together into a single \pkt{REQ} packet. 
There is no logical limit to the number of transactions that may be placed into a single packet, however the Client must obey the \pkt{REQ} and \pkt{CPL} packet size limits advertised by the Server in the \pkt{PRB} response packet.
See Section~\ref{sub:server-components} for more information about Server maximum packet sizes.

\begin{enumsub}
    \item
        \label{clause:transaction-order}
        The Server shall execute transactions in the order they appear in a \pkt{REQ} packet.

    \item
	\label{clause:transaction-max-concat}
        There shall be no logical limit for the number of transactions that may be concatenated in a single packet, however the Client shall limit the transactions such that the maximum \pkt{REQ} and \pkt{CPL} packet sizes of the Server are not exceeded.
        \begin{enumsubsub}
            \item 
                The total bytes in a \pkt{REQ} packet shall not exceed the value  \bit{MAX\_REQ} as advertised by the Server.
            \item
                The Client shall not concatenate request transactions such that the total transaction completion sizes will create a \pkt{CPL} packet that exceed the value \bit{MAX\_CPL} as advertised by the Server.
            \begin{example}
                 Example: Say that \bit{MAX\_REQ} = \bit{MAX\_CPL} = 512 bytes (UDP payload).
                 A Client is to concatenate two read request transactions, each for 256 bytes of data each, into a \pkt{REQ} packet.
                 The \pkt{CPL} packet will contain two read completion transactions, each with 256 bytes of data, in addition to 4 bytes of global header, and two transaction headers (with address) of 8 bytes each. 
                 The \pkt{CPL} will have a total of 4 + (8 + 256) + (8 + 256) = 532 bytes. 
                 This exceeds the maximum \pkt{CPL} packet size by 20 bytes.
                 The two read request transactions must therefore be separated into two \pkt{REQ} packets.
            \end{example}

        \end{enumsubsub}

\end{enumsub}




% TODO : Examples of concatenated packets

%TODO : Move this to an 'example' subsection at the bottom of the 'Packet Contents' subsection
%Figure~\ref{fig:axioe-packet-med} illustrates a packet containing two write transactions.  

% TODO : TiKZ version!
%% ---- Packet Overview figures
%\begin{figure}[hp!]
%    \centering
%    \includegraphics[width=\figwidthlg]{figures/packet-format/axioe-packet-med}
%    \caption{Example of an AXIoE packet containing two write transactions}
%    \label{fig:axioe-packet-med}
%\end{figure}


