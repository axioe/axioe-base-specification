% ---------------- INTRODUCTION ---------------- %
\section{Introduction}
\label{sec:introduction}
AXIoE is a packet format and network protocol for executing ARM\textsuperscript{\textregistered} AMBA\textsuperscript{\textregistered} 4 AXI standard bus style memory transactions on a remote device connected through an Ethernet network.
AXIoE was designed with the goals of:
\begin{enumerate}
    \item
        Resource efficient server implementations in embedded devices.
    \item
        Reliable and in-order access to the register/memory bus of the remote device to simplify the access patterns and error handling requirements of application software.
    \item
        Efficient use of the network for bulk data transport.
    \item
        Decode and Encode transactions in a serial data streaming manner.
    \item
        Mirror the functionality of the AMBA AXI-4 memory bus.
\end{enumerate}

The name AXIoE is an acronym for the Advanced eXtensible Interface over Ethernet. 
While the target register bus is the AMBA 4 AXI standard bus and the expected packet network is Ethernet, other memory buses and network technologies are not precluded.
In general the register bus will be referred to as the AXIoE bus.

% ---------------- TECHNICAL OVERVIEW ---------------- %
\subsection{Technical Overview}
\label{sub:introduciton-technical-overview}

AXIoE was developed for controlling, monitoring, and distributing data to embedded devices such as FPGA and micro-controllers.

Central to the design of the AXIoE protocol is the asymmetric division of responsibilities between the client and server devices.
The server is kept simple, so that it can be implemented efficiently on resource restricted devices.
The client is necessarily more complex and is assumed to be implemented on less resource constrained devices. 
The client originates all requests, is responsible for keeping track of each request's status, and implementing the error recovery mechanisms. 

The AXIoE packet format encapsulates a sequence of memory read and write transactions.  
A success or failure response is returned to the master for every transaction along with any data.
The AXIoE protocol operates a simple sliding window algorithm to provide these benefits:
\begin{enumerate}
 \item A mechanism to detect and recover from packet loss, miss-ordering, and duplication by the network.
 \item Guaranteed in-order execution of transactions on the target memory bus.
 \item Allow multiple packets to be in flight, improving the throughput on real networks with delay greater than one packet transmission time.
\end{enumerate}

The AXIoE environment contains the following component layers as illustrated in figure~\ref{fig:system_hierarchy}:
\begin{itemize}
 \item Master Application(s) - The source and sink of memory access transactions.
 \item AXIoE Transaction Arbiter - Aggregates the transactions of multiple applications.
 \item AXIoE Client - Packages the transactions and operates the AXIoE Protocol.
 \item Network - Transports the AXIoE packets.
 \item AXIoE Server - Participates in the AXIoE protocol. Unpacking request packets and formulating completion (reply) packets.
 \item AXIoE Bus Master - Executes transactions.
 \item Slave Application(s) - Memory target.
\end{itemize}
Memory accesses from a master application traverse the layers down to the target memory in a slave application. The response to the memory access follows the reverse path back to the originating master application.
\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{figures/hierarchy/system_hierarchy}
	\caption{System hierarchy.}
	\label{fig:system_hierarchy}
\end{figure}

% ---------------- STYLE AND FORMATTING CONVENTIONS ---------------- %
\subsection{Style and Formatting Conventions}
\label{sub:introduction-style-and-formatting-conventions}

The majority of this document is written in the language of a formal specification to provide sharp and clear rules so that different implementations of clients and servers can interoperate.
A compliant implementation is one that implements all mandatory clauses.
Mandatory clauses will include the word \textbf{shall}.
Optional clauses will include the word \textbf{should}.
There are few optional clauses, and an implementation should have a good reason not to implement the optional clause.
Some features are optional, but if an implementation is to support the feature it shall completely support the feature by following all associated mandatory clauses.
The protocol includes a capabilities description structure that allows a client to discover the supported features and parameters of a server.

The following sections describe the style and formatting conventions that are used throughout this document. 

\subsubsection{Numbered Lists}
\label{subsub:introduction-numbered-lists}

Numbered lists contain formal clauses that form this specification. 

%\begin{enumerate}[label=\thesubsubsection.\arabic*.]
\begin{enumsubsub}
    \item
        This item is an example of a formal clause.

        %\begin{enumerate}[label=\thesubsubsection.\arabic*.\alph*, ref=\thesubsubsection.\arabic*.\alph*]
        \begin{enumsubsub}
            \item This item is an example of a formal sub-clause. \label{clause:introduction-example-clause}
        \end{enumsubsub}

    \item
        In this example, this item would also be a formal clause.

\end{enumsubsub}

Formal clauses are referenced by clause number. For example, the sub-clause above would be referenced as
\begin{quote}
    Clause~\ref{clause:introduction-example-clause}a
\end{quote}



%Formal clauses are referenced by the section number, followed by the clause number and if appropriate, the sub-clause number. 
%
%\begin{quote}
%    Section N (clause|sub)
%\end{quote}
%
%For example, the sub-clause above would be referenced as
%
%\begin{quote}
%    Section~\ref{subsub:introduction-numbered-lists}(\ref{clause:introduction-example-clause}).
%\end{quote}


\subsubsection{Bullet Point Lists}
\label{subsub:introduction-bullet-point-lists}

Items may appear as bullet lists. Bullet list items that are not numbered do not form part of the formal specification. This type of list is used where a prose sentence is considered to unwieldy or long.

\begin{itemize}
    \item
        This item is an example of a bullet point that does not form part of the formal specification.

    \item 
        This item is also not part of the formal specification.
        \begin{itemize}
            \item
                It is atypical for a bullet list to have sub-lists, but in the case that a sub-list element does not have a number, it too is not part of the formal specification.
        \end{itemize}

    \item
        This item also forms no part of the formal specification.

\end{itemize}

These types of lists are typically provided to give an overview of a concept, and as such are not referenced. If required, a reference to the section or sub-section in which they appear will be given.


\subsubsection{Example Text}
\label{subsub:introduction-example-text}

Where necessary, a block of example text may be provided to illustrate a concept in the specification. This block of text will begin with the word `Example' in italics, followed by a full colon.

\begin{example}
    Example: Example text appears in italics in an in-line quote. Example text is used to illustrate a concept that appears in this document. Example text, while instructive, is not a formal part of the specification.
\end{example}

Example text will typically appear immediately next to a concept or clause to which is intended to clarify. 


\subsubsection{Tables}
\label{subsub:introduction-tables}

Some information may be grouped into tables. The information presented in a table forms part of the formal specification. 

%\subsection{Vocabulary}
%\label{sub:introduction-vocabulary}
%
%The following section defines the meaning and rules of use of specific words that are applied within this specification. 






