# AXIoE - Advanced eXtensible Interface over Ethernet

![AXIoE Logo](logo/AXIoE_logo.svg)

![Static Badge](https://img.shields.io/badge/Download-PDF-blue?style=for-the-badge&link=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F54446774%2Fjobs%2Fartifacts%2Fmaster%2Fraw%2Faxioe-base.pdf%3Fjob%3Dbuild)

## [W. Kamp, "AXI over Ethernet; a protocol for the monitoring and control of FPGA clusters"](https://ieeexplore.ieee.org/document/8280120)
AXI over Ethernet (AXIoE) is a protocol designed to extend guaranteed in-order accesses to the memory-mapped slave registers on an FPGA to a master CPU that is located elsewhere on a local area network. 
AXIoE was designed for use in the Square Kilometre Array (SKA) radio telescope Central Signal Processors (CSP) to provide monitor and control functionality from a few client machines to hundreds of FPGA over a dedicated 1Gb Ethernet network. 
The protocol features an **asymmetric design** such that the AXIoE server is simple, and implementable on an FPGA with few RAM and logic resources. 
Consequentially the responsibility for all communication error recovery is assumed by the AXIoE client machine. 
This approach is different to the standard choice of TCP, where both the client and server end-points participate equally in the bidirectional error recovery. 
AXIoE introduces **conditional execution** to help minimise the detrimental effect of latency, and improve network throughput. 
The COND bit is used to mark transactions as conditional on the successful execution of the previous transaction. 
This permits a try-catch style of programming that promotes the streaming of groups of bus transactions.

AXIoE was designed with the goals of:
1. Resource efficient server implementations in embedded devices.
2. Reliable and in-order access to the register/memory bus of the remote device to simplify the access patterns and error handling requirements of application software.
3. Efficient use of the network for bulk data transport.
4. Decode and Encode transactions in a serial data streaming manner.
5. Mirror the functionality of the AMBA AXI-4 memory bus.
The name AXIoE is an acronym for the Advanced eXtensible Interface over Ethernet. While the target register bus is the AMBA 4 AXI standard bus and the expected packet network is Ethernet, other memory buses and network technologies are not precluded.

# Technical Overview
AXIoE was developed for controlling, monitoring, and distributing data to embedded devices such as FPGA and micro-controllers.

Central to the design of the AXIoE protocol is the **asymmetric** division of responsibilities between the client and server devices. 
The server is kept simple, so that it can be implemented efficiently on resource restricted devices. 
The client is necessarily more complex and is assumed to be implemented on less resource constrained devices. 
The client originates all requests, is responsible for keeping track of each request’s status, and implementing the error recovery mechanisms.

The AXIoE packet format encapsulates a sequence of memory read and write transactions. 
A success or failure response is returned to the master for every transaction along with any data. 

The AXIoE protocol operates a simple sliding window algorithm to provide these benefits:
1. A mechanism to detect and recover from packet loss, miss-ordering, and duplication by the network.
2. Guaranteed in-order execution of transactions on the target memory bus.
3. Allow multiple packets to be in flight, improving the throughput on real networks with delay greater than
one packet transmission time.

![System Hierarchy](figures/hierarchy/system_hierarchy.svg)

The AXIoE environment contains the following component layers as illustrated in the figure above:
- Master Application(s) - The source and sink of memory access transactions.
- AXIoE Transaction Arbiter - Aggregates the transactions of multiple applications.
- AXIoE Client - Packages the transactions and operates the AXIoE Protocol.
- Network - Transports the AXIoE packets.
- AXIoE Server - Participates in the AXIoE protocol. Unpacking request packets and formulating completion (reply) packets.
- AXIoE Bus Master - Executes transactions.
- Slave Application(s) - Memory target.

Memory accesses from a master application traverse the layers down to the target memory in a slave application. 
The response to the memory access follows the reverse path back to the originating master application.

# References/Citations
*If you make use of this protocol, or it inspires your own, please cite this paper.*

[W. Kamp, "AXI over Ethernet; a protocol for the monitoring and control of FPGA clusters"](https://ieeexplore.ieee.org/document/8280120)

2017 International Conference on Field Programmable Technology (ICFPT), Melbourne, VIC, Australia, 2017, pp. 48-55, doi: 10.1109/FPT.2017.8280120.

# Contributions

Please open an issue.